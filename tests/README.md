# Purpose
Any good software should have unit tests. Unit tests help to ensure changes
from one section of the code did not break other parts.
