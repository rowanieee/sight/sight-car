# init.py
from flask import Flask
app = Flask(__name__)

# blueprint for auth routes in our app
from app.main import main as main_blueprint
app.register_blueprint(main_blueprint)
