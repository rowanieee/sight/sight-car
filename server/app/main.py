from flask import Blueprint, request, render_template
main = Blueprint('main', __name__)

# Command for Pi to follow

global command
command = "Forward"
specialMove = ""
method = "Anarchy"
votes = {
    "Forward":0,
    "Reverse":0,
    "Left":0,
    "Right":0
}

'''
Serve the JSON with the command to follow
{
 "Command" : cmd
 "Special Move" : a fancy command
}
'''
@main.route('/command.json')
def getCommand():
    global command
    if method == "Democratic":
        best = "Forward"
        for key in votes.keys():
            if votes[key] >= votes.get(best, 0):
                best = key
            votes[key] = votes[key]/2
        command = best
    elif method == "Anarchy":
        pass
    return "{" \
                "\"Command\":\"" + command + "\"," \
                "\"Special Move\":\"" + specialMove + "\""\
            "}"

'''
Command path
Sets command sent via POST request
'''
@main.route('/setCommand', methods=['POST'])
def setCommand():
    global command
    resp = request.form.get("Command")
    if method == "Anarchy":
        command = resp
    elif method == "Democratic":
        votes[resp] += 1
    return '', 204

'''
Command path
Sets special move sent via POST request
'''
@main.route('/setSpecial', methods=['POST'])
def setSpecial():
    global specialMove
    specialMove = request.form.get("Special")
    return '', 204

'''
Command path
Sets method
'''
@main.route('/setMethod', methods=['POST'])
def setMethod():
    global method
    method = request.form.get("Method")
    return '', 204

'''
Voting page
'''
@main.route('/')
def index():
    return render_template('index.html')

@main.route('/fancyStuff')
def fancyStuff():
    return render_template('fancyStuff.html')

@main.route("/settings.html")
def settings():
    return render_template("settings.html")

