import json
import urllib.request

'''
Returns the command located at the given URL

! No error handling ! Give the correct URL

::param URL, target URL with JSON
'''
def getCommandFromUrl(URL, debugLevel=1):
    # Get JSON response from url
    jsonurl = urllib.request.urlopen(URL)

    # Read in as String
    jsonString = jsonurl.read().decode()

    # Convert String to JSON
    jsonAsDict = json.loads(jsonString)

    # Show command with debug level = 1
    if debugLevel >= 1:
        print("Command from server: " + str(jsonAsDict))

    return jsonAsDict
