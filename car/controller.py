from commands import getCommandFromUrl
import time

# link to server

SERVER = "http://127.0.0.1:5000/command.json"
# Debug level
DEBUG_LEVEL = 1
SPECIAL_MOVE = "Special Move"
COMMAND = "Command"
COMMANDS = []

while True:
    # Get command from server
    json = getCommandFromUrl(SERVER, debugLevel=DEBUG_LEVEL)
    command = json[COMMAND]
    specialMove = json[SPECIAL_MOVE]

    '''
    Process Command
    '''

    '''
    Give time to act on command
    '''
    time.sleep(2)
